#
# Cookbook Name:: devops-challenge
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

### Installing tomcat 7###
package 'tomcat7' do
	action :install
end


cookbook_file '/var/lib/tomcat7/webapps/java-artifact-chef-test.war' do
	source "java-chef-test.war"
	mode "0644"
	notifies :restart, "service[tomcat7]"
end


service 'tomcat7' do
	supports :restart => true
end
