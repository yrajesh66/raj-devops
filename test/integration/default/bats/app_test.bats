# test/integration/default/bats/app_running.bats
@test "app folder exists" {
  ls /var/lib/tomcat7/webapps/java-artifact-chef-test.war
}

@test "app is running" {
  HOST=localhost:8080
  URI=$HOST/java-artifact-chef-test/chef/ping
  curl  -v $URI | grep -i 'Hello'
}
